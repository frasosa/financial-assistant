import streamlit as st
import pickle
import os
from streamlit_chat import message
from streamlit_extras.add_vertical_space import add_vertical_space
from streamlit_extras.colored_header import colored_header
from langchain import PromptTemplate, HuggingFaceHub, LLMChain
from langchain.chains.question_answering import load_qa_chain
from langchain.text_splitter import RecursiveCharacterTextSplitter 
from langchain.embeddings.huggingface_hub import HuggingFaceHubEmbeddings
from langchain.vectorstores import FAISS
from PyPDF2 import PdfReader
from dotenv import load_dotenv

# load env variables
load_dotenv()
# set page title
st.set_page_config(page_title="OpenAssistant Chat App")

# sidebar
with st.sidebar:
    st.title("This is a sidebar")
    st.markdown(
        ''' There is text here that will explain things most liekly
        https://huggingface.co/OpenAssistant/oasst-sft-4-pythia-12b-epoch-3.5
        '''
    )
    add_vertical_space(3)
    st.write("Made by FA ")

def main():
    # header
    st.header("Your personal financial advisor")

    # PDF uploader
    pdf = st.file_uploader("Upload your PDF", type='pdf')

    # there is a pdf uploaded
    if pdf:
        # if the file dump exists on disk we can just load the vector store 
        pdf_name = pdf.name[:-4]
        if os.path.exists(f"data/{pdf_name}.pkl"):
            with open(f"data/{pdf_name}.pkl", "rb") as f:
                VectorStore = pickle.load(f)
        else:
            # make a pdf reader object
            pdf_reader = PdfReader(pdf)

            # extract text from pdf
            text = ""
            for page in pdf_reader.pages:
                text += page.extract_text()

            # create text splitter
            text_splitter = RecursiveCharacterTextSplitter(
                chunk_size = 1000,
                chunk_overlap = 200,
                length_function = len
            )

            # using rext splitter, split text into chuncks
            chunks = text_splitter.split_text(text=text)

            # create embeddings from chunks and store in vector database
            # embeddings are numeric respresentations of the text 
            VectorStore = FAISS.from_texts(chunks, embedding=HuggingFaceHubEmbeddings())

            # dump the embeddings as a pickle file so that we dont have to recompute 
            # if the user enters the same file
            with open(f"data/{pdf_name}.pkl", "wb") as f:
                pickle.dump(VectorStore, f)

        # take user input
        query = st.text_input("Ask a question about your PDF:")
        if query:
            # find relevelant documents using a similarity search
            docs = VectorStore.similarity_search(query=query)

            # get llm
            llm = HuggingFaceHub(repo_id="OpenAssistant/oasst-sft-4-pythia-12b-epoch-3.5", model_kwargs={"max_new_tokens":500})
            # make chain
            chain = load_qa_chain(llm=llm, chain_type="stuff")
            # get response
            response = chain.run(input_documents=docs, question= f"<|prompter|>{query}<|endoftext|>|assistant|>")
            # output response to user
            st.write(response)


# runs main function on app start
if __name__ == '__main__':
    main()


# def main():
#     # header 
#     st.header("Your personal financial advisor")

#     # Assistant response
#     if 'generated' not in st.session_state:
#         st.session_state['generated'] = ["Hi, how can I help you?"]

#     if 'user' not in st.session_state:
#         st.session_state['user'] = ['Hi']

#     response_container = st.container()
#     colored_header(label='', description='', color_name='blue-30')
#     input_container = st.container()

#     def get_input():
#         return st.text_input("You: ", "", key="input")

#     with input_container:
#         user_input = get_input()

#     # setups up the llm and returns a chain used to get responses
#     def chain_setup():
#         # input specification for open assistant llm
#         template = "<|prompter|>{question}<|endoftext|>|assistant|>"

#         # create prompt using the template
#         prompt = PromptTemplate(template=template, input_variables=["question"])

#         # get llm from hugging face
#         llm = HuggingFaceHub(repo_id="OpenAssistant/oasst-sft-4-pythia-12b-epoch-3.5", model_kwargs={"max_new_tokens":1200})

#         # return chain that combines the llm and the prompt
#         return LLMChain(
#             llm=llm,
#             prompt=prompt
#         )
    
#     llm_chain = chain_setup()

#     # main loop
#     with response_container:
#         if user_input:
#             response = llm_chain.run(user_input)
#             st.session_state.user.append(user_input)
#             st.session_state.generated.append(response)
#         if st.session_state['generated']:
#             for i in range(len(st.session_state['generated'])):
#                 message(st.session_state['user'][i], is_user=True, key=str(i) + '_user')
#                 message(st.session_state["generated"][i], key=str(i))