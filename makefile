# creates python environment for you to run the app
env:
	python3 -m venv myenv
	source myenv/bin/activate
	pip install -r requirements.txt

# installs requirements into environment
reqs:
	pip install -r requirements.txt

# runs the app 
app:
	streamlit run app.py
