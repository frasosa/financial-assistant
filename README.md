# Open Assistant App
A quick implentation of a chatbot using the Open Assistant API from huggingface hub

## Setup
After cloning the repo make sure to run make env to install the requirements into your virtual environment
Additionally you will need to make a .env file with a api key for the huggingfacehub api

## Running
You can use the make app command to run the app locally

